# SNS Lambda - Stopped Docker Notification

## Usage

```terraform
module "stopped_docker_notification" {
  source   = "git::https://gitlab.com/itcrowdarg-public/infrastructure/terraform-modules/aws/sns-lambda-stopped-docker-notification?ref=X.Y"

  region             = var.region
  project_name       = var.project_name
  notify_cluster     = "production" # comma separated list of cluster names, blank for all clusters
  slack_webhook_urls = "https://hooks.slack.com/services/XXX/YYY/ZZZ" # Slack webhook URLs, comma separated
}
```