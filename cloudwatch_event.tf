resource "aws_cloudwatch_event_rule" "docker_stopped" {
  name        = "docker_stopped"
  description = "Notification when ECS docker stopped"

  event_pattern = <<EOF
{
  "source": ["aws.ecs"],
  "detail-type": ["ECS Task State Change"],
  "detail": {
    "lastStatus": ["STOPPED"],
    "stoppedReason": [
      {
        "prefix": "Task failed ELB health checks"
      },
      "Essential container in task exited",
      "Task failed container health checks"
    ]
  }
}
EOF
}

resource "aws_cloudwatch_event_target" "sns" {
  rule      = aws_cloudwatch_event_rule.docker_stopped.name
  target_id = "SendToSNS"
  arn       = aws_sns_topic.docker_task_stopped.arn
}
