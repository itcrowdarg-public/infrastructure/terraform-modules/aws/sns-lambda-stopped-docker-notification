locals {
  lambda_src_path = "${path.module}/lambda"
}

data "archive_file" "lambda_source_package" {
  type        = "zip"
  source_dir  = local.lambda_src_path
  output_path = "${path.module}/.tmp/lambda.zip"
}


resource "aws_lambda_function" "it_crowd_slack" {
  filename         = data.archive_file.lambda_source_package.output_path
  function_name    = "it_crowd_slack_notification_sns"
  role             = aws_iam_role.execution_role.arn
  handler          = "function.lambda_handler"
  runtime          = "ruby2.7"
  source_code_hash = data.archive_file.lambda_source_package.output_base64sha256

  environment {
    variables = {
      PROJECT_NAME       = var.project_name
      SLACK_WEBHOOK_URLS = var.slack_webhook_urls
      CLUSTERS           = var.notify_cluster
    }
  }
}

resource "aws_lambda_permission" "with_sns" {
  statement_id  = "AllowExecutionFromSNS"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.it_crowd_slack.function_name
  principal     = "sns.amazonaws.com"
  source_arn    = aws_sns_topic.docker_task_stopped.arn
}
