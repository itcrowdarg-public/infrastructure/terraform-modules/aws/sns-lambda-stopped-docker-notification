require "json"
require "uri"
require "net/http"

def lambda_handler(event:, context:)
  # Parse message from SNS
  @ecs_message = JSON.parse(event["Records"][0]["Sns"]["Message"])

  # If CLUSTERS is blank, notify for all clusters
  notify_clusters = ENV.fetch('CLUSTERS', '').split(',').map { |cl| cl.strip }.uniq
  return true if !notify_clusters.empty? && !notify_clusters.include?(cluster)

  # Project name
  project_name = ENV.fetch("PROJECT_NAME", 'no project informed')

  # Slack webhooks
  webhook_urls = ENV.fetch("SLACK_WEBHOOK_URLS",nil).to_s.split(",").map { |url| url.strip }.uniq

  alert_icon = production? ? "<!here> :red_circle: *PRODUCTION!* - " : ":large_yellow_circle:"

  # Create message
  message = "#{alert_icon} ECS Dockers *STOPPED* - Project `#{project_name}`\n" \
  "AWS account: `#{@ecs_message["account"]}`\n" \
  "ECS Service: `#{@ecs_message["detail"]["group"].split(':').last}`\n" \
  "Cluster: `#{cluster}`\n" \
  "Dockers: \n```#{@ecs_message["detail"]["containers"].map { |c| "#{c['name']} - #{c['image']}" }.join("\n")}```\n" \
  "Reason:\n```#{@ecs_message["detail"]["stoppedReason"]}```" \

  webhook_urls.each do |webhook_url|
    notify_slack(webhook_url, message)
  end
  { statusCode: 200, body: JSON.generate('Slack message sent') }
end

private

def production?
  cluster == "production"
end

def cluster
  @cluster ||= @ecs_message["detail"]["clusterArn"].split('/').last.to_s.strip.downcase
end

def notify_slack(url, message)
  msg = "This is a test message send"
  body = { text: message }.to_json

  url = URI(url)
  http = Net::HTTP.new(url.host, url.port)
  http.use_ssl = true
  http.verify_mode = OpenSSL::SSL::VERIFY_NONE
  request = Net::HTTP::Post.new(url)
  request.body = "payload=#{body}"
  http.request(request)
end