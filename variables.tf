variable "region" {
  description = "AWS region"
  type        = string
}

variable "project_name" {
  description = "Project name"
  type        = string
}

variable "notify_cluster" {
  description = "Cluster to notify, comma separated list of cluster names, blank for all clusters"
  type        = string
}

variable "slack_webhook_urls" {
  description = "Slack webhook URLs, comma separated"
  type        = string
}
